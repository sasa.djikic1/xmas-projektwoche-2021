![IBZ Logo](pictures/ibzlogo.svg "IBZ Logo")

# xMas Projektwoche 2021

Als ein Informatiker ist es sehr wichtig, da immer neue Methoden, Programmiersprachen, Programme etc entwickelt werden, sich auf den neusten Stand weiterzubilden. In dieser Projektwoche lernen wir die zwei Websiten **GitHub** und **GitLab**, die Anzeigesprache **Markdown**, sowie die Methoden von **DevOps.**

## Inhaltsverzeichnis


1. [GitHub](#github)
2. [GitLab](#gitlab)
3. [Markdown](#markdown)
4. [DevOps](#devops)

## GitHub
---

### <strong>Was ist GitHub?</strong>
![IBZ Logo](pictures/githublogo.png)


GitHub ist eine Website, bei dem Menschen, die mit der IT zu tun haben, ihre Softwareversionen hochladen und teilen können. Ein anderer kann diese Softwareversionen anschauen, herunterladen, bearbeiten und wieder hochladen. Auf GitHub kann man Repositories erstellen. Repositories sind Datenbanken, auf denen Codes hochgeladen werden können etc.

### Warum GitHub?

GitHub ist eine einfache Lösung für die gemeinsame Arbeit an einem oder mehrere Projekte gleichzeitig. Die kostenlose Version von GitHub genügt für einfache Zwecke bereits aus und man kann in dieser Version Public-Code-Repositories hosten, was sehr beliebt ist, aber keine Private-Code-Repositories, da dies dann kostenpflichtig ist.

### Welches sind die Vor- und Nachteile von GitHub?

Vorteile:

    -   kostenlose Version genügt
    -   Online
    -   schöne Umgebung
    -   für Bewerbungen gut
    -   Kommentar-Tracking


Nachteile:

    -   falls kein Internet = nicht zugreifbar
    -   gehört Microsoft
    -   nicht unendlich viel Platz
    -   private Repositories sind kostenpflichtig
    -   kein kostenloses Hosten auf Server

### Wozu braucht man GitHub?

Man kann sein Code immer wieder hochladen und herunterladen. Andere Personen können dann mein Code auch anschauen und herunterladen. Ich habe z.B dies genutzt als ich ein Ethereum-Miner eingerichtet habe. Ich konnte so per wget https://github.com/Beispiel herunterladen und direkt nutzen.

### Wie kann ich von GitHub profitieren?

Da es kostenlos ist und sehr viele andere Personen GitHub auch nutzen, ist es mir sehr gelegen, GitHub auch zu nutzen. Ich kann z.B mein Code veröffentlichen und bei einer Bewerbung mein Link zum GitHub-Profil beilegen. Arbeite ich später an einem Projekt mit mehreren Personen kann ich unser Code hochladen und jeder in meiner Gruppe kann dann unser Code herunterladen, bearbeiten und für uns wieder hochladen.

### GitHub und Code auch für Systemtechniker geeignet?

Ich denke auch Informatiker, die eher wenig mit Programmieren zu tun haben, können von GitHub profitieren, da wie bereits geschrieben, Scripts und mehr per Befehl herunterladen kann, um z.B einen Bitcoin-Miiner einzurichten. Auch wenn Applikationsentwickler einen Programm schreiben und dieser fachmännisch getestet werden muss, kann so ein Systemtechniker den Code per GitHub herunterladen und testen, sowie direkt auf GitHub einen Feedback abgeben oder per Ticketsystem.

## GitLab
---
### Was ist GitLab?
![IBZ Logo](pictures/gitlablogo.png)

GitLab ist auch Website, ähnlich wie GitHub, bei dem Menschen, die mit der IT zu tun haben, ihre Softwareversionen hochladen und teilen können. Ein anderer kann diese Softwareversionen anschauen, herunterladen, bearbeiten und wieder hochladen. Auf GitLab kann auch man Repositories erstellen.

### Warum GitLab?

Die kostenlose Version von GitLab verfügt über mehr Funktionen als die von GitHub. GitLab hat ein Wiki, bei dem man vieles nachlesen kann. GitLab verfügt auch über ein Issue-Tracking-System mit Kanban-Tafel, bei dem man Tickets einreichen kann und diese nach Zeit bearbeitet werden. Mithilfe der Kanban-Tafel wird dieser Prozess sogar visualisiert.

### Welches sind die Vor- und Nachteile?

Vorteile:

    -   kostenlose Version genügt
    -   Online
    -   schöne Umgebung
    -   gut für Bewerbungen
    -   hat eine eigene Wiki
    -   private Repositories sind kostenlos
    -   kostenloses Hosten auf Server

<br>
Nachteile:

    -   falls kein Internet = nicht zugreifbar
    -   gehört Microsoft
    -   nicht unendlich viel Platz
    -   kein Kommentar-Tracking

### Wozu braucht man GitLab?

Um Softwareprojekte für entweder privates oder öffentliches Publikum hochzuladen. Der Gegenüber kann dann diese Projekte anschauen, herunterladen und wieder hochladen. Ist gut geeignet für Projekte in einer Firma oder Schule, sowie wenn man sich bewirbt.

### Wie kann ich von GitLab profitieren?

Mit GitLab habe ich Übersicht von meinen Projekten und kann auch in einer Gruppenarbeit Informationen mit anderen austauschen, sowie komme ich besser bei Firmen an, wenn ich GitLab benutze.

### GitHub und Code auch für Systemtechniker geeignet?

Ich bin mir ehrlich gesagt nicht sicher, ob ich GitLab gebrauchen könnte.

## Markdown
---

### Was ist Markdown?
![IBZ Logo](pictures/markdownlogo.png)
<br>

Markdown ist eine Auszeichnungssprache, die sehr an HTML ähnelt. MD ist sehr vereinfacht und wurde 2004 von Aaron Swartz und John Gruber entworfen. In diesem Dokument, welches Sie gerade lesen, wurde in Markdown geschrieben. Markdown wurde entworfen, damit man Webdokumente auch ohne das Überfluten von Klammern etc formatieren kann.

### Warum Markdown

Markdown ist sehr einfach und mal was anderes als immer Word, Word und Word. Mit wenig Erfahrung kann man bereits ein relativ elegant aussehendes Dokument erstellen, welches bestimmt einen guten Eindruck hinterlässt.

### Welches sind die Vor- und Nachteile?


Vorteile:

    -   einfach
    -   elegant
    -   schnell
    -   mal was neues

<br>
Nachteile:

    -   benötigt ein wenig Übung
    -   könnte ungewohnt wirken
    -   Wenn man HTML bereits kann, könnte Markdown redundant sein

### Wozu braucht man Markdown

Markdown benutzt man, um Webseiten zu formatieren mithilfe HTML, jedoch in vereinfachter Form, ohne Klammern und spezielle Syntaxen. So kann man mit wenig Übung schnelle Webdokumente erstellen, die gar nicht mal so schlecht aussehen. Ich überlege mir in Zukunft Markdown zu nutzen, um einfach mal was anderes zu haben als immer nur Office.

### Wie kann ich von Markdown profitieren?

Ich denke eher weniger, da ich bereits Erfahrungen in HTML gesammelt habe. Ich habe das Gefühl, dass man mit HTML mehr erreichen kann als Markdown. Für mich sieht es so aus, als wäre Markdown speziell jetzt für Dokumente gedacht und nicht "richtige Webseiten".

## DevOps
---

### Was ist DevOps?
![IBZ Logo](pictures/devopslogo.png)

DevOps sind Methoden, bei dem Softwareentwickler und Systemtechniker zusammenarbeiten können. Mithilfe dieser Methoden sollte die Zusammenarbeit schneller, effizienter und einfacher geschehen.

### Warum DevOps?

Mit DevOps arbeitet man in der Regel schneller, effizienter und einfacher mit Arbeitskollegen zusammen. Die Entwicklung des Projektes verlauft somit in einer besseren Qualität.

### Welches sind die Vor- und Nachteile?

Vorteile:

    -   schnell
    -   effizient
    -   direkt mit erfahrene Infomratiker arbeiten
    -   wird überall genutzt

<br>
Nachteile:

    -   man muss sich auf andere verlassen können
    -   man muss andere vertrauen können
    -   Man benötigt gewisse Erfahrung
    -   Training ist erforderlich

### Wozu braucht man DevOps?

Damit die Chemie zwischen Systemtechniker und Applikationsentwickler stimmt und während ein oder mehrere Projekte die Entwicklung ohne Probleme verlauft. So kann man sich mit einer Methode effizient und einfach Informationen austauschen.

### Wie kann ich von DevOps profitieren?

Sobald ich im Praktikum bin, kann ich als Systemtechniker mich besser mit Applikationsentwickler verständigen und so auf einem effizienten und einfachen Arbeitsverlauf hinaufarbeiten. Wenn ich DevOps noch besser studieren würde, könnte ich noch viel effizienter und schneller mit Profis zusammenarbeiten.

[zurück zum Anfang](#xmas-projektwoche-2021)