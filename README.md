# xMas Projektwoche 2021

Als ein Informatiker ist es sehr wichtig, da immer neue Methoden, Programmiersprachen, Programme etc entwickelt werden, sich auf den neusten Stand weiterzubilden. In dieser Projektwoche lernen wir die zwei Websiten **GitHub** und **GitLab**, die Anzeigesprache **Markdown**, sowie die Methoden von **DevOps.**
